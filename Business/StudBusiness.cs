﻿using scyllaela.Models;
using Cassandra.Data.Linq;
namespace scyllaela.Business;

public class StudBusiness: IStudBusiness
{
    private ScyllaDBService scylla_service;
    public StudBusiness()
    {
        scylla_service = new ScyllaDBService();
    }

    // make async task
    public async Task<List<stone_data>> GetData()
    {
                                        // call async fetch
        IEnumerable<stone_data> result = await scylla_service.mapper.FetchAsync<stone_data>("SELECT * FROM stone_data");
        return result.ToList();  
    }

}
